//[Section] MongoDB Aggregation

	/*
		-to generate and perform operations to create filtered results that helps us analyze the data
	*/

	/* using aggregate method:
		-the "$match" is used to pass the documents that meet the specified condtion/condtions to the next stage or aggregation process
		Syntax:
			{$match:{field: value}}
		*/

	db.fruits.aggregate([ {$match : {onSale: true}}]);
	/*
		-The group is used to group the elements together and field-value the data from the grouped element
		syntax:
		{$group: _id: "fieldSetGroup"}
	*/

	db.fruits.aggregate([ 
		{$match : {onSale: true}},
		{$group:{_id:"$supplier_id", totalFruits: {$sum: "$stock"}}}
		]);

	//Mini activity:
	// First get all the the fruits that are color Yellow then grouped them into their respective supplier and their available stocks.
	
	db.fruits.aggregate([
		{
			$match: {color: "Yellow"}
		},
		{
			$group: {
				_id: "$supplier_id",
				totalYellowFruits: {$sum: "$stocks"}
			}
		}
		]);

	//Field Projection with aggregation
		/*
			-$project can be used when aggregationmg data to include / exclude from the returned result

			Syntax:
				{$project: {field: 1/0}}
		*/

		db.fruits.aggregate([ 
			{$match : {onSale: true}},
			{$group:{_id:"$supplier_id", totalFruits: {$sum: "$stock"}}},
			{$project: {_id:0}}
			]);

		// Sorting aggregated results
			/*
				- $sort can be used to change the order of the aggregated result

			*/

		db.fruits.aggregate([ 
			{$match : {onSale: true}},
			{$group:{_id:"$supplier_id", totalFruits: {$sum: "$stock"}}},
			{$project: {_id:0}},
			{$sort: {totalFruits: 1}}
			]);

		/*the value in sort

			1 - lowest to highest
			-1 -  highest to lowest*/


		//aggreagating results based on an array fields
			/*
				the $unwind deconstructs an array field from a collection /field with an array value to output a result
			*/

		db.fruits.aggregate([
			{$unwind: "$origin"},
			{$group: {_id:"$origin", fruits{$sum:1}}}
			]);

		//[Section]Other aggregate stages
		//$count all yellow fruits
		db.fruits.aggregate([
					{$match: {color: "Yellow"}},
					{$count: "Yellow Fruits"}
					])
		//$avg gets the average value of the stock

		db.fruits.aggregate([
				{$match: { color: "Yellow"}},
				{$group: {_id: "$color", avgYellow: {$avg: "$stock"}}}
			])

		//$min && $max

			db.fruits.aggregate([
				{$match: { color: "Yellow"}},
				{$group: {_id: "$color", lowestStock: {$min: "$stock"}}}
			])

			db.fruits.aggregate([
				{$match: { color: "Yellow"}},
				{$group: {_id: "$color", highestStock: {$max: "$stock"}}}
			]);
		
		

